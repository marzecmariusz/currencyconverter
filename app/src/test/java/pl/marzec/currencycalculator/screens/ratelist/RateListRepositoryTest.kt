package pl.marzec.currencycalculator.screens.ratelist

import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import io.reactivex.subjects.SingleSubject
import org.junit.Before
import org.junit.Test
import pl.marzec.currencycalculator.*
import pl.marzec.currencycalculator.api.ExchangeApi
import pl.marzec.currencycalculator.model.Resource
import pl.marzec.currencycalculator.model.api.ExchangeApiDto
import pl.marzec.currencycalculator.model.local.ExchangeTable
import pl.marzec.currencycalculator.screens.main.ExchangeRatesUtils

class RateListRepositoryTest : BaseTest() {

    val exchangeApi = mock<ExchangeApi>()
    lateinit var repo: RateListRepository

    @Before
    override fun setUp() {
        super.setUp()
        repo = RateListRepository(exchangeApi)
    }

    @Test
    fun loadingRatesTest() {
        val aSubject = SingleSubject.create<List<ExchangeApiDto>>()
        val bSubject = SingleSubject.create<List<ExchangeApiDto>>()

        whenever(exchangeApi.getTable("a")).thenReturn(aSubject)
        whenever(exchangeApi.getTable("b")).thenReturn(bSubject)

        val observer = mock<Observer<Resource<List<ExchangeTable>>>>()
        repo.loadRates().observeForever(observer)

        aSubject.onSuccess(listOf(createExchangeApiDto(table = "A", rates = listOf(createRateDto(code = "PLN", currency = "Złoty", mid = 1.0)))))
        bSubject.onSuccess(listOf(createExchangeApiDto(table = "B", rates = listOf(createRateDto(code = "EUR", currency = "Euro", mid = 4.2)))))

        verify(observer, times(2)).onChanged(Resource.loading())
        verify(observer).onChanged(
                Resource.success(listOf(
                        createTable(
                                table = "A",
                                rates = listOf(
                                        ExchangeRatesUtils.PLN_RATE,
                                        createRate(currency = "Złoty", code = "PLN", mid = 1.0)
                                )
                        ),
                        createTable(
                                table = "B",
                                rates = listOf(
                                        ExchangeRatesUtils.PLN_RATE,
                                        createRate(currency = "Euro", code = "EUR", mid = 4.2))
                        )
                ))
        )
    }
}