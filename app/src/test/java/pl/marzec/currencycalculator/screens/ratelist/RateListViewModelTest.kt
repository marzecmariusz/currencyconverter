package pl.marzec.currencycalculator.screens.ratelist

import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.reset
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyZeroInteractions
import org.junit.Before
import org.junit.Test
import pl.marzec.currencycalculator.BaseTest

class RateListViewModelTest : BaseTest() {

    private val repo = mock<RateListRepository>()
    lateinit var rateListViewModel: RateListViewModel

    @Before
    override fun setUp() {
        super.setUp()
        rateListViewModel = RateListViewModel(repo)
    }

    @Test
    fun showDialogOpenTest() {
        val observer = mock<Observer<List<String>>>()
        rateListViewModel.showTableDatesAction.observeForever(observer)

        rateListViewModel.showDates()
        verify(observer).onChanged(listOf())
    }
}