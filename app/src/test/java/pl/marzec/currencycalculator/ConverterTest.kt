package pl.marzec.currencycalculator

import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import pl.marzec.currencycalculator.model.local.Rate
import pl.marzec.currencycalculator.screens.main.Converter
import pl.marzec.currencycalculator.screens.main.ExchangeRatesUtils

/**
 * Created by mariuszmarzec on 13/02/2017.
 */
class ConverterTest {

    @Test
    @Throws(Exception::class)
    fun convertFromPlnToOther() {
        Assert.assertEquals(0.23201856148, Converter.convert(pln, euro, 1.0), DELTA)
        Assert.assertEquals(0.24691358024, Converter.convert(pln, dollar, 1.0), DELTA)
    }

    @Test
    @Throws(Exception::class)
    fun convertFromOtherToPln() {
        Assert.assertEquals(4.31, Converter.convert(euro, pln, 1.0), DELTA)
        Assert.assertEquals(4.05, Converter.convert(dollar, pln, 1.0), DELTA)
    }

    @Test
    @Throws(Exception::class)
    fun convertFromOtherToOther() {
        Assert.assertEquals(1.06419753086, Converter.convert(euro, dollar, 1.0), DELTA)
    }

    companion object {

        private const val DELTA = 0.0000000001
        lateinit var euro: Rate
        lateinit var pln: Rate
        lateinit var dollar: Rate

        @BeforeClass
        @Throws(Exception::class)
        @JvmStatic
        fun setUp() {
            pln = ExchangeRatesUtils.PLN_RATE

            euro = Rate("EURO", "EURO", 4.31)

            dollar = Rate("USD", "USD", 4.05)
        }
    }

}