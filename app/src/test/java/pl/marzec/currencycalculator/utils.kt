package pl.marzec.currencycalculator

import pl.marzec.currencycalculator.common.emptyString
import pl.marzec.currencycalculator.model.api.ExchangeApiDto
import pl.marzec.currencycalculator.model.api.RateDto
import pl.marzec.currencycalculator.model.local.ExchangeTable
import pl.marzec.currencycalculator.model.local.Rate

fun createExchangeApiDto(
        table: String? = null,
        no: String? = null,
        effectiveDate: String? = null,
        rates: List<RateDto>? = null

): ExchangeApiDto {
    return ExchangeApiDto(table, no, effectiveDate, rates)
}

fun createRateDto(currency: String? = null, code: String? = null, mid: Double? = null): RateDto {
    return RateDto(currency, code, mid)
}

fun createRate(currency: String = emptyString(), code: String = emptyString(), mid: Double = 1.0): Rate {
    return Rate(currency, code, mid)
}

fun createTable(
        table: String = emptyString(),
        number: String = emptyString(),
        date: String = emptyString(),
        rates: List<Rate> = emptyList()
): ExchangeTable {
    return ExchangeTable(
            table,
            number,
            date,
            rates
    )
}