package pl.marzec.currencycalculator.listeners

import android.text.Editable
import android.text.TextWatcher
import android.view.View

class FocusableTextWatcher(
        private val textWatcher: TextWatcher
) : View.OnFocusChangeListener, TextWatcher {

    private var focused = false

    override fun onFocusChange(view: View, hasFocus: Boolean) {
        focused = hasFocus
    }

    override fun afterTextChanged(text: Editable) {
        if (focused) {
            textWatcher.afterTextChanged(text)
        }
    }

    override fun beforeTextChanged(text: CharSequence, start: Int, count: Int, after: Int) {
        if (focused) {
            textWatcher.beforeTextChanged(text, start, count, after)
        }
    }

    override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        if (focused) {
            textWatcher.onTextChanged(text, start, before, count)
        }
    }

}

