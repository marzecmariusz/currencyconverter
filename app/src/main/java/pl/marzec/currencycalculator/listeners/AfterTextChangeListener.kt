package pl.marzec.currencycalculator.listeners

import android.text.Editable
import android.text.TextWatcher

abstract class AfterTextChangeListener : TextWatcher {

    abstract override fun afterTextChanged(text: Editable)

    override fun beforeTextChanged(text: CharSequence, start: Int, count: Int, after: Int) = Unit

    override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) = Unit
}