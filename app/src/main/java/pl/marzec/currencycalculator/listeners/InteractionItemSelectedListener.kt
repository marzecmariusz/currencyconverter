package pl.marzec.currencycalculator.listeners

import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView

class InteractionItemSelectedListener(
        private val onItemSelectedListener: AdapterView.OnItemSelectedListener
) : View.OnTouchListener, AdapterView.OnItemSelectedListener {

    var touched = false

    override fun onTouch(view: View, event: MotionEvent): Boolean {
        touched = true
        return false
    }

    override fun onNothingSelected(adapter: AdapterView<*>?) {
        if (touched) {
            onItemSelectedListener.onNothingSelected(adapter)
            touched = true
        }
    }

    override fun onItemSelected(adapter: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (touched) {
            onItemSelectedListener.onItemSelected(adapter, view, position, id)
            touched = true
        }
    }

}

