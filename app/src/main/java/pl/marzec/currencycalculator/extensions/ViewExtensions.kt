package pl.marzec.currencycalculator.extensions

import android.view.View
import android.view.View.*

fun View.visible() {
    visibility = VISIBLE
}

fun View.gone() {
    visibility = GONE
}

fun View.setVisibility(visible: Boolean, invisibleAllowed: Boolean = false) {
    val invisibleState = if (invisibleAllowed) INVISIBLE else GONE
    visibility = if (visible) VISIBLE else invisibleState
}