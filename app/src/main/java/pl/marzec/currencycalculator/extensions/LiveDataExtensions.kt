package pl.marzec.currencycalculator.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

fun <T1: Any, T2: Any, R: Any> mapNotNull(source1: LiveData<T1>, source2: LiveData<T2>, action: (T1, T2) -> R?): LiveData<R> {
    return MediatorLiveData<R>().apply {
        addSource(source1) {
            whenNotNull(it, source2.value) { it1, it2 ->
                value = action(it1, it2)
            }
        }
        addSource(source2) {
            whenNotNull(source1.value, it) { it1, it2 ->
                value = action(it1, it2)
            }
        }
    }
}

fun <T1: Any, T2: Any, R: Any> whenNotNull(ob: T1?, ob2: T2?, block: (T1, T2)->R?): R? =
        if (ob != null && ob2 != null) block(ob, ob2) else null
