package pl.marzec.currencycalculator.lifecycle

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import pl.marzec.currencycalculator.extensions.orFalse

class NetworkStateObserver(val context: Context, listener: OnConnectionStateListener) : LifecycleObserver {

    private val networkConnectionReceiver = NetworkConnectionReceiver(listener)

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun connectListener() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(networkConnectionReceiver, intentFilter)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun disconnectListener() {
        context.unregisterReceiver(networkConnectionReceiver)
    }


    private class NetworkConnectionReceiver(val listener: OnConnectionStateListener) : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            context?.let {
                if (isNetworkAvailable(context))
                    listener.onConnected()
                else
                    listener.onDisconnected()
            }
        }

        private fun isNetworkAvailable(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = cm.activeNetworkInfo
            return networkInfo?.isConnected.orFalse()
        }
    }

    interface OnConnectionStateListener {
        fun onConnected()
        fun onDisconnected() = Unit
    }
}