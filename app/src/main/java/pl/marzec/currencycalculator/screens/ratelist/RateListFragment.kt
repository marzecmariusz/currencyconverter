package pl.marzec.currencycalculator.screens.ratelist

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_rate_list.*
import pl.marzec.currencycalculator.R
import pl.marzec.currencycalculator.base.BaseDaggerFragment
import pl.marzec.currencycalculator.databinding.FragmentRateListBinding

class RateListFragment : BaseDaggerFragment<FragmentRateListBinding>() {

    private lateinit var viewModel: RateListViewModel

    override fun getLayoutId(): Int {
        return R.layout.fragment_rate_list
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        viewModel = obtainViewModel()
        viewDataBinding.lifecycleOwner = this
        viewDataBinding.viewModel = viewModel

        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

        observeNonNull(viewModel.showTableDatesAction) {
            val message: String = it.fold(StringBuilder()) { sb, value ->
                sb.append(value).append("\n")
            }.toString()
            AlertDialog.Builder(activity!!)
                    .setTitle(R.string.rate_list)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_rate_list, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.item_info -> {
                viewModel.showDates()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
