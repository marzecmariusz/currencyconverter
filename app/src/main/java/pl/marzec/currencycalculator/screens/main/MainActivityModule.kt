package pl.marzec.currencycalculator.screens.main

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.marzec.currencycalculator.di.FragmentScope

@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindConverterFragment(): ConverterFragment
}