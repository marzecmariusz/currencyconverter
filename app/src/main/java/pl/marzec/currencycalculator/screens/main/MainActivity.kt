package pl.marzec.currencycalculator.screens.main

import pl.marzec.currencycalculator.R
import pl.marzec.currencycalculator.base.BaseDaggerActivity
import pl.marzec.currencycalculator.databinding.ActivityMainBinding


class MainActivity : BaseDaggerActivity<ActivityMainBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }
}

