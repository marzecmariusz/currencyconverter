package pl.marzec.currencycalculator.screens.ratelist

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import kotlinx.android.synthetic.main.view_rate.view.*
import pl.marzec.currencycalculator.base.BaseViewHolder
import pl.marzec.currencycalculator.common.NotifyingRecyclerAdapterDelegate
import pl.marzec.currencycalculator.model.local.Rate

class RatesAdapter : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder>() {

    var rates by NotifyingRecyclerAdapterDelegate(listOf<Rate>())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return RateViewHolder(RateView(parent.context).apply {
            layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
        })
    }

    override fun getItemCount() = rates.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    inner class RateViewHolder(private val view: RateView) : BaseViewHolder(view) {

        @SuppressLint("SetTextI18n")
        override fun onBind(position: Int) {
            val rate = rates[position]
            view.currency.text = "${rate.code} - ${rate.currency}"
            view.rate.text = rate.mid.toString()
        }
    }
}