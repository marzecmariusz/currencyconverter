package pl.marzec.currencycalculator.screens.main

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.widget.AppCompatSpinner
import android.text.Editable
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.fragment_converter.*
import org.jetbrains.anko.contentView
import pl.marzec.currencycalculator.R
import pl.marzec.currencycalculator.base.BaseDaggerFragment
import pl.marzec.currencycalculator.common.emptyString
import pl.marzec.currencycalculator.common.whenAllNotNull
import pl.marzec.currencycalculator.databinding.FragmentConverterBinding
import pl.marzec.currencycalculator.listeners.AfterTextChangeListener
import pl.marzec.currencycalculator.listeners.FocusableTextWatcher
import pl.marzec.currencycalculator.listeners.InteractionItemSelectedListener
import pl.marzec.currencycalculator.model.Loading
import pl.marzec.currencycalculator.model.Resource
import pl.marzec.currencycalculator.model.Error
import pl.marzec.currencycalculator.model.Success
import pl.marzec.currencycalculator.model.local.Rate
import pl.marzec.currencycalculator.screens.ratelist.RateListActivity
import pl.marzec.currencycalculator.viewmodel.NonNullObserver

class ConverterFragment : BaseDaggerFragment<FragmentConverterBinding>() {

    private var snackbar: Snackbar? = null
    private lateinit var viewModel: MainViewModel

    override fun getLayoutId(): Int {
        return R.layout.fragment_converter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = obtainViewModel()
        setHasOptionsMenu(true)
        viewDataBinding.lifecycleOwner = this
        viewDataBinding.viewModel = viewModel

        observeNonNull(viewModel.currencies) { res: Resource<List<Rate>> ->
            when (res) {
                is Success -> onDataLoaded(res)
                is Error -> showLoadingError()
                is Loading -> hideSnackbar()
            }
        }

        observe(viewModel.openRateAction) {
            startActivity(RateListActivity.newIntent(activity!!))
        }

        setUp()

        // TODO UNCOMMENT LIFECYCLE
//        val networkStateObserver = NetworkStateObserver(context!!, object : NetworkStateObserver.OnConnectionStateListener {
//            override fun onConnected() {
//                activity?.toast(R.string.toast_network_connected)
//            }
//
//            override fun onDisconnected() {
//                activity?.toast(R.string.toast_network_disconnected)
//            }
//        })

//        lifecycle.addObserver(networkStateObserver)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_rate_list -> {
                viewModel.openRateList()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showLoadingError() {
        snackbar(R.string.error_occured, R.string.try_again_button) { loadExchangeData() }
    }

    private fun onDataLoaded(res: Success<List<Rate>>) {
        firstCurrencySpinner.adapter = createAdapter(res.data)
        secondCurrencySpinner.adapter = createAdapter(res.data)
        setCurrentCurrency(firstCurrencySpinner, viewModel.firstCurrency)
        setCurrentCurrency(secondCurrencySpinner, viewModel.secondCurrency)
        firstAmountEditText.setText(viewModel.firstAmount)
        updateCurrencyRate(firstCurrencySpinner, secondCurrencySpinner, currencyRate)
        update(firstAmountEditText, firstCurrencySpinner, secondAmountEditText, secondCurrencySpinner)
    }

    private fun loadExchangeData() {
        viewModel.loadCurrencyRates()
    }

    private fun setCurrentCurrency(currencySpinner: AppCompatSpinner, currency: String?) {
        val index = viewModel.currencies.value?.data?.indexOfFirst { it.code == currency } ?: -1
        currencySpinner.setSelection(if (index > -1) index else 0)
    }

    private fun snackbar(messageResId: Int, buttonText: Int, onButtonClickListener: (Snackbar) -> Unit) {
        snackbar?.dismiss()
        snackbar = Snackbar.make(activity!!.contentView!!, messageResId, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(buttonText) {
                onButtonClickListener(this)
                dismiss()
            }
            show()
        }
    }

    private fun hideSnackbar() {
        snackbar?.dismiss()
    }

    private fun setUp() {
        firstAmountEditText.addTextChangedListener(object : AfterTextChangeListener() {
            override fun afterTextChanged(text: Editable) {
                viewModel.firstAmount = text.toString()
            }
        })

        setUpListeners(firstAmountEditText, firstCurrencySpinner, OnValueChangeListener(
                firstAmountEditText,
                firstCurrencySpinner,
                secondAmountEditText,
                secondCurrencySpinner,
                currencyRate,
                true
        ))

        setUpListeners(secondAmountEditText, secondCurrencySpinner, OnValueChangeListener(
                secondAmountEditText,
                secondCurrencySpinner,
                firstAmountEditText,
                firstCurrencySpinner,
                currencyRate
        ))
    }

    private fun setUpListeners(amountEditText: EditText, currencySpinner: AppCompatSpinner, valueListener: OnValueChangeListener) {
        amountEditText.isSaveEnabled = false
        val firstWatcher = FocusableTextWatcher(valueListener)
        amountEditText.onFocusChangeListener = firstWatcher
        amountEditText.addTextChangedListener(firstWatcher)

        val firstOnItemSelectedListener = InteractionItemSelectedListener(valueListener)
        currencySpinner.setOnTouchListener(firstOnItemSelectedListener)
        currencySpinner.onItemSelectedListener = firstOnItemSelectedListener
    }

    private fun createAdapter(data: List<Rate>): ArrayAdapter<Rate> {
        return ArrayAdapter<Rate>(context!!, android.R.layout.simple_spinner_item, data).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
    }

    private fun updateCurrencyRate(firstCurrency: Spinner, secondCurrency: Spinner, currencyRate: TextView) {
        val firstRate = getCurrentRate(firstCurrency)
        val secondRate = getCurrentRate(secondCurrency)
        listOf(firstRate, secondRate).whenAllNotNull {
            val result = Converter.convert(it[0], it[1], 1.0)
            currencyRate.text = String.format("1 %s = %f %s", it[0].code, result, it[1].code)
        }
    }

    private fun update(firstAmount: EditText,
                       firstCurrency: Spinner,
                       secondAmount: EditText,
                       secondCurrency: Spinner) {
        val firstRate = getCurrentRate(firstCurrency)
        val secondRate = getCurrentRate(secondCurrency)
        val firstValue = getAmount(firstAmount)
        if (!listOf(firstRate, secondRate, firstValue).whenAllNotNull {
                    val result = Converter.convert(it[0] as Rate, it[1] as Rate, it[2] as Double)
                    secondAmount.setText(result.toString())
                }) {
            secondAmount.setText(emptyString())
        }
    }

    private fun getAmount(editText: EditText) = editText.text.toString().toDoubleOrNull()

    private fun getCurrentRate(spinner: Spinner) = (spinner.selectedItem as? Rate)

    inner class OnValueChangeListener(
            private val firstAmount: EditText,
            private val firstCurrency: Spinner,
            private val secondAmount: EditText,
            private val secondCurrency: Spinner,
            private val currencyRate: TextView,
            private val isFirst: Boolean = false
    ) : AfterTextChangeListener(), AdapterView.OnItemSelectedListener {

        private var currencyChanged = false

        override fun afterTextChanged(text: Editable) {
            if (!currencyChanged) {
                update(firstAmount, firstCurrency, secondAmount, secondCurrency)
            }
        }

        override fun onNothingSelected(adapter: AdapterView<*>?) = Unit

        override fun onItemSelected(adapter: AdapterView<*>?, view: View, position: Int, id: Long) {
            currencyChanged = true
            update(firstAmount, firstCurrency, secondAmount, secondCurrency)
            if (isFirst) {
                viewModel.firstCurrency = (firstCurrency.selectedItem as? Rate)?.code.orEmpty()
                updateCurrencyRate(firstCurrency, secondCurrency, currencyRate)
            } else {
                viewModel.secondCurrency = (firstCurrency.selectedItem as? Rate)?.code.orEmpty()
                updateCurrencyRate(secondCurrency, firstCurrency, currencyRate)
            }
            currencyChanged = false
        }
    }
}