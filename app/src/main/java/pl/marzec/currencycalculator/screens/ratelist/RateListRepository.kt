package pl.marzec.currencycalculator.screens.ratelist

import androidx.lifecycle.LiveData
import io.reactivex.Single
import pl.marzec.currencycalculator.api.ExchangeApi
import pl.marzec.currencycalculator.extensions.mapNotNull
import pl.marzec.currencycalculator.model.Resource
import pl.marzec.currencycalculator.model.Success
import pl.marzec.currencycalculator.model.local.ExchangeTable
import pl.marzec.currencycalculator.repository.NetworkBoundResource
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RateListRepository @Inject constructor(
        private val api: ExchangeApi
) {

    fun loadRates(table: String): LiveData<Resource<ExchangeTable>> {
        return object : NetworkBoundResource<ExchangeTable>() {
            override fun getFromApi(): Single<ExchangeTable> {
                return api.getTable(table).map {
                    ExchangeTable(it.first())
                }
            }
        }.get()
    }

    fun loadRates(): LiveData<Resource<List<ExchangeTable>>> {
        return mapNotNull(loadRates("a"), loadRates("b")) { aTable, bTable ->
            if (aTable.isLoading || bTable.isLoading) {
                Resource.loading()
            } else if (aTable is Success && bTable is Success) {
                Resource.success(listOf(aTable.data, bTable.data))
            } else {
                Resource.error(aTable.throwable ?: bTable.throwable ?: UnknownHostException())
            }
        }
    }
}
