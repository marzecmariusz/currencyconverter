package pl.marzec.currencycalculator.screens.ratelist

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.marzec.currencycalculator.di.FragmentScope

@Module
abstract class RateListActivityModule {

    @ContributesAndroidInjector
    @FragmentScope
    internal abstract fun bindRateListFragment(): RateListFragment

}
