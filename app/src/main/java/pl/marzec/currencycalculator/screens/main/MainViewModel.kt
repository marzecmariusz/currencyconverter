package pl.marzec.currencycalculator.screens.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import pl.marzec.currencycalculator.api.CurrencyRepository
import pl.marzec.currencycalculator.extensions.orFalse
import pl.marzec.currencycalculator.model.Resource
import pl.marzec.currencycalculator.model.local.ExchangeTable
import pl.marzec.currencycalculator.model.local.Rate
import pl.marzec.currencycalculator.viewmodel.EmptySingleLiveEvent
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repository: CurrencyRepository) : ViewModel() {

    private val trigger = EmptySingleLiveEvent()
    private val exchangeTables: LiveData<Resource<List<ExchangeTable>>>
    val openRateAction = EmptySingleLiveEvent()
    val currencies: LiveData<Resource<List<Rate>>>
    var firstCurrency: String = "PLN"
    var secondCurrency: String = "EUR"

    var firstAmount: String = "1"

    init {
        exchangeTables = Transformations.switchMap(trigger) {
            repository.getCurrencyTables()
        }
        currencies = Transformations.map(exchangeTables) {
            it.map {
                it.flatMap { it.rates }.toHashSet().toList()
            }
        }
    }

    @Inject
    fun loadCurrencyRates() {
        val value = currencies.value
        if (!value?.isLoading.orFalse() && !value?.isSuccess.orFalse()) trigger.call()
    }

    fun openRateList() {
        openRateAction.call()
    }
}