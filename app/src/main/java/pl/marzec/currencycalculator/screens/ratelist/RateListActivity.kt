package pl.marzec.currencycalculator.screens.ratelist

import android.content.Context
import android.content.Intent
import pl.marzec.currencycalculator.R
import pl.marzec.currencycalculator.base.BaseDaggerActivity
import pl.marzec.currencycalculator.databinding.ActivityRateListBinding

class RateListActivity : BaseDaggerActivity<ActivityRateListBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_rate_list
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, RateListActivity::class.java)
        }
    }
}
