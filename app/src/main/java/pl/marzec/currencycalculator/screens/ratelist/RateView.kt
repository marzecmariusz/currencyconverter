package pl.marzec.currencycalculator.screens.ratelist

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import org.jetbrains.anko.dip
import pl.marzec.currencycalculator.R

class RateView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.view_rate, this)
        val padding = dip(10)
        setPadding(padding, padding, padding, padding)
        gravity = Gravity.CENTER_VERTICAL
        orientation = HORIZONTAL
    }
}