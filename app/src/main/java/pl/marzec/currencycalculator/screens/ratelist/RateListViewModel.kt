package pl.marzec.currencycalculator.screens.ratelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import pl.marzec.currencycalculator.common.emptyString
import pl.marzec.currencycalculator.extensions.mapNotNull
import pl.marzec.currencycalculator.model.Success
import pl.marzec.currencycalculator.viewmodel.EmptySingleLiveEvent
import javax.inject.Inject

class RateListViewModel @Inject constructor(
    private val rateListRepository: RateListRepository
): ViewModel() {

    private val loadingDataTrigger = EmptySingleLiveEvent()
    private var tablesDates = emptyList<String>()
    val showTableDatesAction = MutableLiveData<List<String>>()
    val searchText = MutableLiveData<String>().apply { value = emptyString() }

    private val tables = Transformations.switchMap(loadingDataTrigger) {
        Transformations.map(rateListRepository.loadRates()) { res ->
            if (res is Success) {
            tablesDates = res.data.map { table -> table.table + ": " + table.date }
            }
            res
        }
    }

    private val ratesInternal = Transformations.map(tables) { resource ->
        resource.map { table -> table.flatMap { it.rates } }
    }
    val rates = mapNotNull(ratesInternal, searchText) { res, searchedText ->
        res.map { rates ->
            rates.filter { val searched = searchedText.toLowerCase()
                it.currency.toLowerCase().contains(searched) ||
                        it.code.toLowerCase().contains(searched) }
        }
    }

    @Inject
    fun loadData() {
        loadingDataTrigger.call()
    }

    fun showDates() {
        showTableDatesAction.value = tablesDates
    }

    fun resetSearch() {
        searchText.value = emptyString()
    }
}
