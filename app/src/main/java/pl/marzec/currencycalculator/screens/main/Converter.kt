package pl.marzec.currencycalculator.screens.main

import pl.marzec.currencycalculator.model.local.Rate

object Converter {

    private val BASE = ExchangeRatesUtils.PLN_RATE

    fun convert(current: Rate, target: Rate, amount: Double) = when {
        isBase(current) -> fromBaseToTarget(target, amount)
        isBase(target) -> fromCurrentToBase(current, amount)
        else -> {
            val asBase = fromCurrentToBase(current, amount)
            fromBaseToTarget(target, asBase)
        }
    }

    private fun fromBaseToTarget(target: Rate, amount: Double) = amount / target.mid

    private fun fromCurrentToBase(base: Rate, amount: Double) = base.mid * amount

    private fun isBase(base: Rate) = BASE.code.equals(base.code, ignoreCase = true)
}