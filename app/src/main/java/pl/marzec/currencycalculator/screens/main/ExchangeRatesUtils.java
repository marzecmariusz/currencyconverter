package pl.marzec.currencycalculator.screens.main;

import pl.marzec.currencycalculator.Constants;
import pl.marzec.currencycalculator.model.local.Rate;

public class ExchangeRatesUtils {

  public static final Rate PLN_RATE = getPlnRate();

  private static Rate getPlnRate() {
    return new Rate("Złoty (Polska)", Constants.POLISH_CURRENCY_CODE, 1);
  }
}