package pl.marzec.currencycalculator.viewmodel

import androidx.lifecycle.Observer

class NonNullObserver<T>(private val action: (T) -> Unit) : Observer<T> {

    override fun onChanged(t: T?) {
        t?.let { action(t) }
    }
}