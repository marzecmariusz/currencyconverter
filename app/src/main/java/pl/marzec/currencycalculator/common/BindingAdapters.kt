package pl.marzec.currencycalculator.common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import pl.marzec.currencycalculator.model.Resource
import pl.marzec.currencycalculator.model.local.Rate
import pl.marzec.currencycalculator.screens.ratelist.RatesAdapter

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("rates")
    fun setRates(view: RecyclerView, rates: Resource<List<Rate>>?) {
        val adapter = (view.adapter as? RatesAdapter) ?: RatesAdapter().apply { view.adapter = this }
        adapter.rates = rates?.data.orEmpty()
    }
}
