package pl.marzec.currencycalculator.common

import androidx.recyclerview.widget.RecyclerView
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class NotifyingRecyclerAdapterDelegate<T>(var value: T) : ReadWriteProperty<RecyclerView.Adapter<out androidx.recyclerview.widget.RecyclerView.ViewHolder>, T> {
    override fun getValue(thisRef: androidx.recyclerview.widget.RecyclerView.Adapter<out RecyclerView.ViewHolder>, property: KProperty<*>): T {
        return value
    }

    override fun setValue(thisRef: androidx.recyclerview.widget.RecyclerView.Adapter<out RecyclerView.ViewHolder>, property: KProperty<*>, value: T) {
        this.value = value
        thisRef.notifyDataSetChanged()
    }
}