package pl.marzec.currencycalculator.common

fun <T: Any> Collection<T?>.whenAllNotNull(block: (List<T>)-> Unit): Boolean {
    if (this.all { it != null }) {
        block(this.filterNotNull())
        return true
    }
    return false
}

fun emptyString() = ""