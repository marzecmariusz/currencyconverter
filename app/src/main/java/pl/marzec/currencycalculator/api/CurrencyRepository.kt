package pl.marzec.currencycalculator.api

import androidx.lifecycle.LiveData
import androidx.room.EmptyResultSetException
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import pl.marzec.currencycalculator.model.Resource
import pl.marzec.currencycalculator.model.api.ExchangeApiDto
import pl.marzec.currencycalculator.model.db.ExchangeDao
import pl.marzec.currencycalculator.model.db.TableWithAllRates
import pl.marzec.currencycalculator.model.local.ExchangeTable
import pl.marzec.currencycalculator.model.local.Rate
import pl.marzec.currencycalculator.repository.CacheNetworkBoundResource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrencyRepository @Inject constructor(
        private val exchangeDao: ExchangeDao,
        private val exchangeApi: ExchangeApi) {


    fun getCurrencyTables(): LiveData<Resource<List<ExchangeTable>>> {
        return object : CacheNetworkBoundResource<List<ExchangeTable>>() {
            override fun createSaveDbCall(data: List<ExchangeTable>): Completable {
                return Completable.fromCallable { exchangeDao.updateRates(data) }
            }

            override fun createDbCall(): Single<List<ExchangeTable>> {
                return exchangeDao.getAllExchangeTables()
                        .map { list: List<TableWithAllRates> ->
                            Observable.fromArray(list)
                                    .flatMapIterable { it }
                                    .map {
                                        ExchangeTable(
                                                it.table!!.table,
                                                it.table!!.number,
                                                it.table!!.date,
                                                Rate.from(it.rates)
                                        )
                                    }.blockingIterable().toList()
                        }
                        .doOnNext { list ->
                            if (list.isEmpty()) {
                                throw EmptyResultSetException("No currencies rates in database")
                            }
                        }
                        .firstOrError()
            }

            override fun createApiCall(): Single<List<ExchangeTable>> {
                return Flowable.zip(
                        exchangeApi.getTable("a").toFlowable(),
                        exchangeApi.getTable("b").toFlowable(),
                        BiFunction { t1: List<ExchangeApiDto>, t2: List<ExchangeApiDto> ->
                            t1.toMutableList().apply { addAll(t2) }.toList()
                        })
                        .map { list: List<ExchangeApiDto> -> list.map { ExchangeTable(it) } }
                        .firstOrError()
            }

            override fun shouldSaveToDb() = true

        }.get()
    }
}
