package pl.marzec.currencycalculator.api

import android.content.Context
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import pl.marzec.currencycalculator.common.isOnline
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.ConnectException

fun provideExchangeApi(context: Context): ExchangeApi {
    val retrofit = Retrofit.Builder()
            .baseUrl("http://api.nbp.pl/api/")
            .client(provideConnectionCheck(provideJsonClient(), context))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    return retrofit.create(ExchangeApi::class.java)
}

fun provideConnectionCheck(client: OkHttpClient, context: Context): OkHttpClient {
    return client.newBuilder().addInterceptor { chain ->
        if (!isOnline(context)) {
            throw ConnectException()
        }
        chain.proceed(chain.request())
    }.build()
}

fun createDefaultOkHttpClient() = OkHttpClient.Builder()

private fun provideJsonClient(): OkHttpClient {
    val client = createDefaultOkHttpClient()
    client.interceptors().add(Interceptor { chain ->
        chain.proceed(chain.request().newBuilder()
                .addHeader("Accept", "application/json")
                .build())
    })
    return client.build()
}