package pl.marzec.currencycalculator.api

import io.reactivex.Flowable
import io.reactivex.Single
import pl.marzec.currencycalculator.model.api.ExchangeApiDto
import retrofit2.http.GET
import retrofit2.http.Path


interface ExchangeApi {

    @GET("exchangerates/tables/{table}/")
    fun getTable(@Path("table") table: String): Single<List<ExchangeApiDto>>
}

