package pl.marzec.currencycalculator.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pl.marzec.currencycalculator.screens.main.MainViewModel
import pl.marzec.currencycalculator.screens.ratelist.RateListViewModel
import pl.marzec.currencycalculator.viewmodel.CustomViewModelFactory

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindUserViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RateListViewModel::class)
    abstract fun bindRateListViewModel(viewModel: RateListViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: CustomViewModelFactory): ViewModelProvider.Factory
}
