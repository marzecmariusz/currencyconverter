package pl.marzec.currencycalculator.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.marzec.currencycalculator.screens.main.MainActivity
import pl.marzec.currencycalculator.screens.main.MainActivityModule
import pl.marzec.currencycalculator.screens.ratelist.RateListActivity
import pl.marzec.currencycalculator.screens.ratelist.RateListActivityModule


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    @ActivityScope
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [RateListActivityModule::class])
    @ActivityScope
    internal abstract fun bindRateListActivity(): RateListActivity
}
