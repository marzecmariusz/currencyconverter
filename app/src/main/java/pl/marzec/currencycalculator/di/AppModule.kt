package pl.marzec.currencycalculator.di

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.marzec.currencycalculator.App
import pl.marzec.currencycalculator.database.ExchangeDatabase
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: App): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideExchangeApi(context: Context) = pl.marzec.currencycalculator.api.provideExchangeApi(context)

    @Provides
    @Singleton
    fun provideDatabase(context: Context): ExchangeDatabase = ExchangeDatabase.getDatabase(context)

    @Provides
    @Singleton
    fun provideExchange(database: ExchangeDatabase) = database.getExchangeDao()

}