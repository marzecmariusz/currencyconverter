package pl.marzec.currencycalculator.di

import javax.inject.Scope

@Scope
@Retention
annotation class FragmentScope
