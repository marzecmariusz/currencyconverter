package pl.marzec.currencycalculator.model.local

import org.joda.time.DateTime

data class Cache<T>(val data: T, val dateTime: DateTime, val isCache: Boolean = false)