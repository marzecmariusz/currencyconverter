package pl.marzec.currencycalculator.model.local

import androidx.room.Ignore
import pl.marzec.currencycalculator.model.api.ExchangeApiDto
import pl.marzec.currencycalculator.screens.main.ExchangeRatesUtils

data class ExchangeTable (
        val table: String,
        val number: String,
        val date: String,
        val rates : List<Rate>
) {
    @Ignore
    constructor(exchangeTable: ExchangeApiDto) : this(
            exchangeTable.table.orEmpty(),
            exchangeTable.no.orEmpty(),
            exchangeTable.effectiveDate.orEmpty(),
            exchangeTable.rates
                    ?.filter {
                        it.code != null && it.currency != null && it.mid != null
                    }
                    ?.map {
                        Rate(it.currency.orEmpty(), it.code.orEmpty(), it.mid ?: .0)
                    }
                    .orEmpty()
                    .toMutableList()
                    .apply {
                        add(0, ExchangeRatesUtils.PLN_RATE)
                    })
}