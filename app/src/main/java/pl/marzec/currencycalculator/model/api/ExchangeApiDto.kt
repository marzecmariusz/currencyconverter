package pl.marzec.currencycalculator.model.api

import pl.marzec.currencycalculator.model.api.RateDto

data class ExchangeApiDto(
        val table: String? = null,
        val no: String? = null,
        val effectiveDate: String? = null,
        val rates : List<RateDto>? = null
)