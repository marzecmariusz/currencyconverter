package pl.marzec.currencycalculator.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import pl.marzec.currencycalculator.common.emptyString
import pl.marzec.currencycalculator.model.local.ExchangeTable

@Entity
class ExchangeTableDb(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var table: String = emptyString(),
        var number: String = emptyString(),
        var date: String = emptyString()
) {
    constructor(ob: ExchangeTable) : this(
            0,
            ob.table,
            ob.number,
            ob.date
    )
}