package pl.marzec.currencycalculator.model.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import io.reactivex.Flowable
import pl.marzec.currencycalculator.model.local.ExchangeTable

@Dao
abstract class ExchangeDao {

    @Transaction
    @Query("SELECT * from ExchangeTableDb")
    abstract fun getAllExchangeTables(): Flowable<List<TableWithAllRates>>

    @Transaction
    open fun updateRates(exchangeTables: List<ExchangeTable>) {
        removeAllRates()
        removeAllTables()
        for (exchangeTable in exchangeTables) {
            val exchangeTableDb = ExchangeTableDb(exchangeTable)
            val rates = RateDb.from(exchangeTable.rates)
            insert(exchangeTableDb, rates)
        }
    }

    @Transaction
    open fun insert(exchangeTable: ExchangeTableDb, rates: List<RateDb>) {
        val tableId = insert(exchangeTable)
        rates.forEach { it.tableId = tableId }
        insertAllRates(rates)
    }

    @Insert
    abstract fun insert(exchangeTable: ExchangeTableDb): Long

    @Insert
    abstract fun insertTables(exchangeTable: List<ExchangeTableDb>)

    @Insert
    abstract fun insertAllRates(rates: List<RateDb>)

    @Query("DELETE FROM ExchangeTableDb")
    abstract fun removeAllTables()

    @Query("DELETE FROM RateDb")
    abstract fun removeAllRates()
}