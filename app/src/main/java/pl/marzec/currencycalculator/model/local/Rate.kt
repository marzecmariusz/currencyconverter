package pl.marzec.currencycalculator.model.local

import androidx.room.Ignore
import pl.marzec.currencycalculator.model.db.RateDb

data class Rate(var currency: String, var code: String, var mid: Double) {

    @Ignore
    constructor(ob: RateDb) : this(
            ob.currency,
            ob.code,
            ob.mid
    )

    override fun toString() = String.format("%s - %s", code, currency)

    companion object {
        fun from(list: List<RateDb>) = list.map { Rate(it) }
    }
}
