package pl.marzec.currencycalculator.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import pl.marzec.currencycalculator.common.emptyString
import pl.marzec.currencycalculator.model.local.Rate

@Entity
data class RateDb(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var currency: String = emptyString(),
        var code: String = emptyString(),
        var mid: Double = 1.0,
        var tableId: Long = 0) {

    constructor(ob: Rate) : this(
            0,
            ob.currency,
            ob.code,
            ob.mid,
            0
    )

    companion object {
        fun from(rates: List<Rate>) = rates.map { RateDb(it) }
    }
}
