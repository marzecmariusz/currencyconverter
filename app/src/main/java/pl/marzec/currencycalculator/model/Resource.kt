package pl.marzec.currencycalculator.model

sealed class Resource<T>(open val data: T?, open val throwable: Throwable?) {

    val isLoading: Boolean
        get() = this is Loading

    val isError: Boolean
        get() = this is Error

    val isSuccess: Boolean
        get() = this is Success

    fun <R> map(mapData: (T) -> R): Resource<R> {
        return when(this) {
            is Success -> Success(mapData(this.data))
            is Loading -> Loading(this.data?.let { mapData(it) })
            is Error -> Error(this.throwable)
        }
    }

    fun copy(): Resource<T> {
        return when(this) {
            is Success -> Success(this.data)
            is Loading -> Loading(this.data)
            is Error -> Error(this.throwable)
        }
    }

    companion   object {

        fun <T> success(data: T): Resource<T> {
            return Success(data)
        }

        fun <T> error(throwable: Throwable, data: T? = null): Resource<T> {
            return Error(throwable, data)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Loading(data)
        }
    }
}

data class Success<T>(override val data: T) : Resource<T>(data, null)

data class Loading<T>(override val data: T? = null) : Resource<T>(data, null)

data class Error<T>(override val throwable: Throwable, override val data: T? = null) : Resource<T>(data, throwable)