package pl.marzec.currencycalculator.model.db

import androidx.room.Embedded
import androidx.room.Relation

class TableWithAllRates {
    @Embedded
    var table: ExchangeTableDb? = null
    @Relation(parentColumn = "id", entityColumn = "tableId")
    var rates: List<RateDb> = emptyList()
}