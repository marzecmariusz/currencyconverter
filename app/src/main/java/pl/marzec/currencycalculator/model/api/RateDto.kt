package pl.marzec.currencycalculator.model.api

data class RateDto(
        val currency: String? = null,
        val code: String? = null,
        val mid: Double? = null
)