package pl.marzec.currencycalculator.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import pl.marzec.currencycalculator.viewmodel.NonNullObserver

abstract class BaseFragment : Fragment() {

    inline fun <T> observeNonNull(data: LiveData<T>, crossinline action: (T) -> Unit) {
        data.observe(this, NonNullObserver { action(it) })
    }

    inline fun <T> observe(data: LiveData<T>, crossinline action: (T?) -> Unit) {
        data.observe(this, Observer { action(it) })
    }
}
