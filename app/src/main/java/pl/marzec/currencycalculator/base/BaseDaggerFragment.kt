package pl.marzec.currencycalculator.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


abstract class BaseDaggerFragment<B : ViewDataBinding> : BaseFragment(), HasSupportFragmentInjector {

    protected lateinit var mRootView: View
    protected lateinit var viewDataBinding: B
    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<androidx.fragment.app.Fragment>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @LayoutRes abstract fun getLayoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        mRootView = viewDataBinding.root
        return mRootView
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun supportFragmentInjector(): DispatchingAndroidInjector<androidx.fragment.app.Fragment>  {
        return this.childFragmentInjector
    }

    protected inline fun <reified T : ViewModel> obtainViewModel() : T {
        val viewModelProvider =
        if(activity is androidx.fragment.app.FragmentActivity) {
            ViewModelProviders.of(activity as androidx.fragment.app.FragmentActivity, viewModelFactory)
        } else {
            ViewModelProviders.of(this, viewModelFactory)
        }

        return viewModelProvider.get(T::class.java)
    }
}