package pl.marzec.currencycalculator.repository

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.marzec.currencycalculator.model.Resource

abstract class CacheNetworkBoundResource<ResultType>(
        private val result: MutableLiveData<Resource<ResultType>> = MutableLiveData()
)
    : NetworkBoundResource<ResultType>() {

    @SuppressLint("CheckResult")
    override fun get(): LiveData<Resource<ResultType>> {
        getFromApi()
                .map { Resource.success(it) }
                .onErrorResumeNext { apiThrowable ->
                    getFromDb()
                            .map { Resource.success(it) }
                            .onErrorResumeNext { Single.just(Resource.error(apiThrowable)) }
                }
                .toFlowable()
                .startWith(Resource.loading())
                .subscribe({
                    result.value = it
                }, {
                    result.value = Resource.error(it)
                })
        return result
    }

    override fun getFromApi(): Single<ResultType> {
        return createApiCall()
                .doOnSuccess {
                    if (shouldSaveToDb()) {
                        saveToDatabase(it)
                    }
                }
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    private fun getFromDb(): Single<ResultType> {
        return createDbCall()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    }

    private fun saveToDatabase(data: ResultType) {
        createSaveDbCall(data)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    abstract fun createSaveDbCall(data: ResultType): Completable

    abstract fun createApiCall(): Single<ResultType>

    abstract fun createDbCall(): Single<ResultType>

    abstract fun shouldSaveToDb(): Boolean

}