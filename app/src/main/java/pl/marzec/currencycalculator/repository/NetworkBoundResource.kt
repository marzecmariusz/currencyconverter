package pl.marzec.currencycalculator.repository

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.marzec.currencycalculator.model.Resource

abstract class NetworkBoundResource<ResultType>(
        private val result: MutableLiveData<Resource<ResultType>> = MutableLiveData()
) {

    abstract fun getFromApi(): Single<ResultType>

    @SuppressLint("CheckResult")
    open fun get(): LiveData<Resource<ResultType>> {
        getFromApi()
                .map { Resource.success(it) }
                .onErrorReturn { Resource.error(it) }
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .toFlowable()
                .startWith(Resource.loading())
                .subscribe({
                    result.value = it
                }, {
                    result.value = Resource.error(it)
                })
        return result
    }

}