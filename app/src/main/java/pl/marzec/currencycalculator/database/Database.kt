package pl.marzec.currencycalculator.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import pl.marzec.currencycalculator.model.db.ExchangeDao
import pl.marzec.currencycalculator.model.db.ExchangeTableDb
import pl.marzec.currencycalculator.model.db.RateDb




@Database(entities = [ExchangeTableDb::class, RateDb::class], version = 1)
abstract class ExchangeDatabase : RoomDatabase() {
    abstract fun getExchangeDao(): ExchangeDao

    companion object {


        private var instance: ExchangeDatabase? = null

        fun getDatabase(context: Context): ExchangeDatabase {
            if (instance == null) {
                synchronized(ExchangeDatabase::class.java) {
                    if (instance == null) {
                        instance = Room.databaseBuilder(context.applicationContext,
                                ExchangeDatabase::class.java, "exchange_database")
                                .build()

                    }
                }
            }
            return instance!!
        }

    }
}